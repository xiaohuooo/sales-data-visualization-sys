import vue from '@/main.js'
// 绘制饼图分析用户下单时间段占比
const charts_user = () => {
    var Time_ratio = vue.$echarts.init(document.getElementById("Time_ratio"));
    Time_ratio.showLoading({ text: '加载中...' });
    vue.$axios.get('/time').then(res => {
        Time_ratio.hideLoading();
        // 指定图表的配置项和数据
        Time_ratio.setOption({
            tooltip: {
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            }, //配置提示框组件
            legend: {
                top: '5%',
                type: 'scroll',
                data: res.data.name
            }, //配置图例组件
            calculable: true,
            series: [ //配置数据系列组件
                {
                    name: '访问来源',
                    type: 'pie',
                    radius: '65%', //设置半径
                    center: ['50%', '60%'], //设置圆心
                    clockwise: true,
                    data: res.data.Time_period
                }
            ]
        })
    });

    // 绘制折线柱状混合图分析各月份商品订单量和订单总金额
    var Order_volume_and_amount = vue.$echarts.init(document.getElementById("Order_volume_and_amount"));
    Order_volume_and_amount.showLoading({ text: '加载中...' });
    vue.$axios.get('/amount').then(res => {
        Order_volume_and_amount.hideLoading();
        let data = res.data;
        Order_volume_and_amount.setOption({ //指定图表的配置项和数据
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                top: '3%',
                type: 'scroll',
                data: data.type
            },
            grid: {
                left: "2%",
                right: "2%",
                top: "15%",
                bottom: "0%",
                containLabel: true
            }, //设置画布的上下左右留白
            xAxis: [{
                data: data.months
            }],
            yAxis: [
                //设置两个Y轴之1：订单量
                {
                    type: 'value',
                    name: '销售量（件）'
                },
                //设置两个Y轴之2：金额
                {
                    type: 'value',
                    name: '金额（元）'
                }
            ],
            series: [{
                    name: '销售量',
                    type: 'bar',
                    data: data.Order_volume
                },
                {
                    name: '订单总金额',
                    type: 'line',
                    yAxisIndex: 1, //指定使用第2个y轴
                    data: data.total_amount
                }
            ]
        })
    });

    // 绘制柱状图分析各用户类型的数量分布
    var The_user_type = vue.$echarts.init(document.getElementById("The_user_type"));
    The_user_type.showLoading({ text: '加载中...' });
    vue.$axios.get('/user').then(res => {
        let data = res.data;
        The_user_type.hideLoading();
        //指定图表的配置项和数据
        The_user_type.setOption({
            tooltip: { //配置提示框组件
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: "2%",
                right: "2%",
                top: "20%",
                bottom: "0%",
                containLabel: true
            }, //设置画布的上下左右留白
            xAxis: {
                data: data.month
            }, //配置x轴坐标系
            yAxis: {}, //配置y轴坐标系
            series: [ //配置数据系列
                {
                    name: '活跃用户',
                    type: 'bar',
                    data: data.Active_users,
                    markPoint: { //设置标记点
                        data: [{
                            type: 'min',
                            name: '最小值'
                        }]
                    }
                },
                {
                    name: '流失用户',
                    type: 'bar',
                    data: data.Churn_users,
                    markPoint: { //设置标记点
                        data: [{
                            type: 'max',
                            name: '最大值'
                        }]
                    }
                },
                {
                    name: '潜在用户',
                    type: 'bar',
                    data: data.qianzai_users,
                    markLine: { //设置标记线
                        data: [{
                            type: 'average',
                            name: '平均值'
                        }]
                    }
                },
                {
                    name: '一般用户',
                    type: 'bar',
                    data: data.General_users
                }
            ]
        })
    });

    // 绘制堆积面积图分析各月份库存量和进货量
    var Inventory_and_incoming_quantity = vue.$echarts.init(document.getElementById("Inventory_and_incoming_quantity"));
    Inventory_and_incoming_quantity.showLoading({ text: '加载中...' });
    // 绘制折线图分析各月份存货周转天数
    var Inventory_turnover = vue.$echarts.init(document.getElementById("Inventory_turnover"));
    Inventory_turnover.showLoading({ text: '加载中...' });
    vue.$axios.get('/inventory').then(res => {
        let data = res.data
        Inventory_and_incoming_quantity.hideLoading();
        Inventory_turnover.hideLoading();
        //指定绘制堆积面积图分析各月份库存量和进货量图表的配置项和数据
        Inventory_and_incoming_quantity.setOption({
            tooltip: {
                trigger: 'axis'
            }, //配置提示框组件
            legend: {
                top: '5%',
                type: 'scroll',
                data: data.type,
                default: '10'
            }, //配置图例组件
            grid: {
                left: "2%",
                right: "2%",
                top: "20%",
                bottom: "0%",
                containLabel: true
            }, //设置画布的上下左右留白
            xAxis: [{ //配置X轴坐标系
                boundaryGap: false,
                data: data.month
            }],
            yAxis: [{}],
            series: [{ //配置数据系列
                    name: '库存量',
                    type: 'line', //设置指定显示为折线
                    stack: '总量', //smooth:true,
                    areaStyle: {},
                    data: data.Inventory_quantity
                },
                {
                    name: '进货量',
                    type: 'line', //设置指定显示为折线
                    stack: '总量', //设置堆积
                    areaStyle: {},
                    data: data.Incoming_quantity
                }
            ]
        })
        //指定绘制折线图分析各月份存货周转天数图表的配置项和数据
        Inventory_turnover.setOption({
            tooltip: {},
            grid: {
                left: "2%",
                right: "2%",
                top: "10%",
                bottom: "0%",
                containLabel: true
            }, //设置画布的上下左右留白
            xAxis: [{
                data: data.month
            }], //配置X轴坐标轴
            yAxis: [{
                type: 'value'
            }], //配置Y轴坐标轴
            series: [ //配置数据系列
                {
                    name: '存货周转天数',
                    type: 'line', //设置指定显示为折线
                    data: data.Inventory_turnover_days,
                    smooth: true
                }
            ]
        })
    });

    // 绘制条形图分析订单出货状态分布
    var Shipment = vue.$echarts.init(document.getElementById("Shipment"));
    Shipment.showLoading({ text: '加载中...' });
    vue.$axios.get('/status').then(res => {
        Shipment.hideLoading();
        Shipment.setOption({
            tooltip: {
                trigger: 'axis'
            },
            grid: {
                left: "2%",
                right: "2%",
                top: "10%",
                bottom: "0%",
                containLabel: true
            }, //设置画布的上下左右留白
            xAxis: [{}],
            yAxis: [{
                data: res.data.Shipment_status,
                inverse: true //降序排序
            }],
            series: [{
                name: '出货状态',
                type: 'bar',
                color: 'pink', //设置柱形的颜色
                data: res.data.Number_of_occurrences,
            }]
        })
    });

    // 绘制柱状图分析订单支付形式的使用次数
    var Form_payment = vue.$echarts.init(document.getElementById("Form_payment"));
    Form_payment.showLoading({ text: '加载中...' });
    vue.$axios.get('/pay').then(res => {
        Form_payment.hideLoading();
        //指定图表的配置项和数据
        Form_payment.setOption({
            tooltip: {},
            grid: {
                left: "2%",
                right: "2%",
                top: "10%",
                bottom: "0%",
                containLabel: true
            }, //设置画布的上下左右留白
            xAxis: {
                data: res.data.Form_of_payment
            },
            yAxis: {},
            series: [{ //配置系列列表
                name: '支付状态', //设置系列名称
                type: 'bar', //设置类型
                color: 'skyblue', //设置柱形的颜色
                legendHoverLink: true, //设置系列是否启用图例hover时的联动高亮
                itemStyle: {
                    barBorderRadius: [18, 18, 0, 0]
                }, //设置图形的形状
                barWidth: '40', //设置柱形的宽度
                barCategoryGap: '20%', //设置柱形的间距
                data: res.data.Number_of_uses
            }]
        })
    });

    window.onresize = function () {
        Time_ratio.resize();
        Order_volume_and_amount.resize();
        The_user_type.resize();
        Inventory_and_incoming_quantity.resize();
        Inventory_turnover.resize();
        Shipment.resize();
        Form_payment.resize();
    }
}
export default charts_user