import Vue from 'vue'
import VueRouter from 'vue-router'

// 安装VueRouter
Vue.use(VueRouter)
const routes = [{
    path: '/',
    redirect: '/home' // 重定向
    },
    {
    path: '/home',
    name: 'home',
    redirect: '/first', // 重定向
    component: () => import('../layout/Index.vue'),
    children: [{
        //首页
        path: '/first',
        name: 'first',
        component: () => import('../views/first_page.vue'),
        },
        {
        // 订单页
        path: '/sales',
        name: 'sales',
        component: () => import('../views/sales_data.vue'),
        },
        {
        // 库存页
        path: '/subscriber',
        name: 'subscriber',
        component: () => import('../views/subscrber_data.vue'),
        },
        {
        // 可视化页
        path: '/visual',
        name: 'visual',
        component: () => import('../views/visual_interface.vue'),
        }
        ]
    },
]
// 创建router实例，传入routes配置
const router = new VueRouter({
    routes
})
export default router