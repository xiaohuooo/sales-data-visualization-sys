import Vue from 'vue'
import App from './App.vue'
// 导入 路由
import router from './router'
// 导入 ElementUI
import ElementUI from 'element-ui'
// 使用 element-ui 还需要导入一个 css 文件
import 'element-ui/lib/theme-chalk/index.css'
// 引用全局样式文件
import "@/assets/css/resets.css"
// import echarts from 'echarts'  // 引入echarts
import * as echarts from 'echarts'
import axios from 'axios'

// 指向后台服务
axios.defaults.baseURL = window.location.origin
Vue.prototype.$axios = axios // 将axios挂载到原型上

// 安装路由
Vue.use(router)
// 安装 ElementUI
Vue.use(ElementUI)
// 注册为全局属性，组件内使用直接this.$echarts调用
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false

// new Vue({
//   router,
//   render: h => h(App),
// }).$mount('#app')

let vue = new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

export default vue