# sales-data-visualization-sys
npm i element-ui -S
npm install vue-router@3.6.5
npm install echarts
npm install axios
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
